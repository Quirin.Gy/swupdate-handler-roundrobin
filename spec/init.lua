--[[

    SWUpdate Round Robin Handler Tests

    Common test helpers and definitions

    Copyright (C) 2021, Siemens AG
    Author: Christian Storm <christian.storm@siemens.com>

    SPDX-License-Identifier: GPL-2.0-or-later

--]]

--luacheck: no max line length
--luacheck: no global

local assert   = require("luassert")
local stub     = require("luassert.stub")
local swupdate = require("swupdate")


--- Dedent a multi-line string.
--
-- The line prefixed with the least non-tab white space is
-- reset to zero indentation. Thus, the opening line of
-- the string may retain some indentation *if* there are
-- lines of less indentation terminating the string.
--
-- @param str  Multiline string indented consistently.
-- @return Unindented string.
function string.dedent(str)
    str = str:gsub(' +$', ''):gsub('^ +', '')
    local level = math.huge
    local minPrefix = ''
    local len
    for prefix in str:gmatch('\n( +)') do
        len = #prefix
        if len < level then
            level = len
            minPrefix = prefix
        end
    end
    return (str:gsub('\n' .. minPrefix, '\n'):gsub('\n$', ''))
end


--- Load Testee SWUpdate Lua Handler.
--
-- Load the testee Lua SWUpdate handler and expose
-- some local functions/tables to the sandbox.locals
-- table for testability.
--
-- @param sandbox  Environment table to load the testee into.
-- @return load()-returned chunk function.
function load_testee(sandbox)
    function read(file)
        local f = assert(io.open(file, "r"))
        local content = f:read("*all")
        f:close()
        return content
    end
    local chunk = load(
        read("swupdate_handlers_roundrobin.lua").."\n"..
        "locals = { \
            rrconfig = rrconfig, \
            load_config = load_config, \
            parse_csv = parse_csv, \
            strip_dev_path = strip_dev_path, \
            get_dev_path = get_dev_path, \
            PARTTYPE = PARTTYPE, \
            WHEN = WHEN \
        }",
        "testee", "t", sandbox)
    return chunk
end


-- Show table contents in busted's error output.
assert:set_parameter("TableFormatLevel", -1)


--- Busted Matcher for a table's key-value presence.
--
-- Simplistic matcher for key-value pair presence
-- in a (nested) table with globally unique keys.
--
-- Usage example:
--   match.is_present({ dev="blue", type="raw"})
--
-- @param arguments  Table with expected key-value pairs.
-- @param value      Table to check for argument's presence.
-- @return A function returning true if present, false otherwise.
local function is_present(_, arguments)
    return function(value)
        assert(type(arguments[1]) == "table")
        assert(type(value) == "table")
        local function find(tab, needle)
            for k, v in pairs(tab) do
                if k == needle then return v end
                if type(v) == "table" then
                    local result = find(v, needle)
                    if result ~= nil then return result end
                end
            end
        end
        for ek, ev in pairs(arguments[1]) do
            if find(value, ek) ~= ev then return false end
        end
        return true
    end
end
assert:register("matcher", "present", is_present)


-- Common Stubs
stubs = {
    io_open = function(matchlist, journal)
        return stub(io, "open",
            function(path)
                for _, item in pairs(matchlist) do
                    if string.find(path, item[1]) ~= nil then
                        if type(item[2]) == "boolean" and item[2] == false then
                            return nil, "DENIED"
                        end
                        return {
                            open  = function(self, _, _) return self end,
                            read  = function(_, _)
                                        return type(item[2]) == "string" and item[2] or ""
                                    end,
                            write = function(_, ...)
                                        if journal ~= nil then
                                            for _, v in ipairs({...}) do
                                                journal[#journal+1] = v
                                            end
                                        end
                                    end,
                            flush = function()  end,
                            close = function(_) end,
                        }
                    end
                end
            end
            )
    end,
    swupdate_call_handler = function(retcode)
        stub(swupdate, "call_handler",
            function(handler, image)
                if retcode ~= nil then
                    return retcode, "return code override"
                end
                return swupdate.handler[handler](image)
            end
            )
        return swupdate.call_handler
    end,
    swupdate_get_bootenv = function(bootenv)
        stub(swupdate, "get_bootenv",
            function(key)
                for _, item in pairs(bootenv) do
                    if key == item[1] then
                        return item[2]
                    end
                end
                return nil
            end
            )
        return swupdate.get_bootenv
    end
}


-- Common configurations
configurations = {
    raw_cmdline_rr = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=cmdline_rr
            key=root
        ]]),
    raw_bootenv_rr = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=bootenv_rr
            key=root
        ]])
}

