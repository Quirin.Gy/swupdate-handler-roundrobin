--[[

    SWUpdate Lua Module Interface.

    See: https://github.com/sbabic/swupdate/blob/master/corelib/lua_interface.c
    This Lua module is for reference (e.g., code completion) and to track the
    Lua module provided by SWUpdate to Lua handlers.

    Copyright (C) 2021, Siemens AG
    Author: Christian Storm <christian.storm@siemens.com>

    SPDX-License-Identifier: GPL-2.0-or-later

--]]

-- luacheck: no unused args

local swupdate = {
    error = function(msg, ...) end,
    trace = function(msg, ...) end,
    info  = function(msg, ...) end,
    warn  = function(msg, ...) end,
    debug = function(msg, ...) end,

    progress_update = function(percent) end,

    mount  = function(device, filesystem) end,
    umount = function(target) end,

    register_handler = function(name, funcptr, mask) end,
    call_handler     = function(handler, image) end,

    tmpdirscripts = function() end,
    tmpdir        = function() end,

    set_bootenv = function(key, value) end,
    get_bootenv = function(key) end
}

swupdate.RECOVERY_STATUS = {
    IDLE       = 0,
    START      = 1,
    RUN        = 2,
    SUCCESS    = 3,
    FAILURE    = 4,
    DOWNLOAD   = 5,
    DONE       = 6,
    SUBPROCESS = 7
}

swupdate.HANDLER_MASK = {
    IMAGE_HANDLER      = 1,
    FILE_HANDLER       = 2,
    SCRIPT_HANDLER     = 4,
    BOOTLOADER_HANDLER = 8,
    PARTITION_HANDLER  = 16,
    NO_DATA_HANDLER    = 32
}

swupdate.handler = {
    ["archive"]        = function(image) return 0, "OK" end,
    ["tar"]            = function(image) return 0, "OK" end,
    ["bootloader"]     = function(image) return 0, "OK" end,
    ["dummy"]          = function(image) return 0, "OK" end,
    ["flash-hamming1"] = function(image) return 0, "OK" end,
    ["flash"]          = function(image) return 0, "OK" end,
    ["raw"]            = function(image) return 0, "OK" end,
    ["rawfile"]        = function(image) return 0, "OK" end,
    ["rawcopy"]        = function(image) return 0, "OK" end,
    ["rdiff_image"]    = function(image) return 0, "OK" end,
    ["rdiff_file"]     = function(image) return 0, "OK" end,
    ["remote"]         = function(image) return 0, "OK" end,
    ["swuforward"]     = function(image) return 0, "OK" end,
    ["ubivol"]         = function(image) return 0, "OK" end,
    ["ucfw"]           = function(image) return 0, "OK" end,
    ["fail"]           = function(image) return 1, "Deliberate failure." end,
}

return swupdate
