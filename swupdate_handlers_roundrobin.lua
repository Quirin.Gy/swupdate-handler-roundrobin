--[[

    SWUpdate Round Robin Handler.

    Copyright (C) 2021, Siemens AG
    Author: Christian Storm <christian.storm@siemens.com>

    SPDX-License-Identifier: GPL-2.0-or-later

--]]

--luacheck: no max line length
--luacheck: no global


--[[ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ]]
--[[  Compiled- / Built-in Configuration                                       ]]
--[[ <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ]]

-- local configuration = [[ ... ]]


--[[ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ]]
--[[  Round Robin Handler Implementation                                       ]]
--[[ <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ]]

-- require() the SWUpdate Lua module if not loaded.
if not package.loaded["swupdate"] then
    swupdate = require("swupdate")
end

-- Table holding the Round Robin Handler's configuration.
local rrconfig = {
    -- Default configuration file, tried if no built-in config is defined.
    file    = "/etc/swupdate.handler.ini",
    builtin = configuration or nil,
    values  = nil,
}

-- Partition type enum-alike and reverse lookup table.
local PARTTYPE = {
    UUID  = 1, [1] = "PARTUUID=",
    PLAIN = 2, [2] = "/dev/",
    UBI   = 3, [3] = ""
}

-- Pre/post-handler execution point enum-alike to make code more readable.
local WHEN = {
    PRE  = "Pre",
    POST = "Post",
    FAIL = "Fail"
}

-- Name of this handler used as logging output prefix.
local logprefix = "Round Robin Handler"


--- Parse and process the CSV fields of string.
--
-- The result is a Table populated with (1) the CSV items indexed by number
-- and (2) CSV item, i.e., indexing by CVS item gives the sequential number
-- while indexing by number gives the CSV item.
-- If there's a mapping given in sw-description's device= field, it's
-- represented in the 'map' subtable, otherwise it's an identity map.
--
-- @param  csv       A comma separated string.
-- @param  callback  Function to post-process a parsed CSV item [optional].
-- @return Table, or, in case of error, {}.
local function parse_csv(csv, callback)
    if not csv then return {} end
    callback = callback or function(item) return item end
    local dict = { map = {} }
    for item in csv:gmatch("([^,]+)") do
        local device_node, map = item:match("(%g+)%->(%g+)")
        device_node = callback(device_node and device_node or item)
        map = callback(map and map or device_node)
        dict[#dict+1]         = device_node
        dict[device_node]     = #dict
        dict.map[device_node] = map
        dict.map[map]         = device_node
    end
    local mapcount = 0
    for _ in pairs(dict.map) do mapcount = mapcount + 1 end
    if mapcount % #dict ~= 0 then
        swupdate.warn("%s: Mismatch parsing CSV string '%s', expected %d values, got %d.",
                      logprefix, csv, mapcount + (mapcount % #dict), mapcount)
    end
    return dict
end


--- Strip the key= prefix from a key=value string.
--
-- @param  item  <key>=<value> string, e.g., root=/dev/sda.
-- @param  key   Prefix to strip, e.g., "root=".
-- @return The <key>=<value> string (same as input).
--         The <key>= prefix-stripped item input parameter.
local function strip_dev_path(item, key)
    if not item then return nil end
    if     item:find(key .. "PARTUUID=", 1, true) then
        return item, item:sub(10 + #key)
    elseif item:find(key .. "/dev/", 1, true) then
        return item, item:sub(6 + #key)
    elseif item:find(key .. "ubi", 1, true) then
        return item, item:sub(1 + #key)
    else
        return item, item
    end
end


--- Return the full /dev/... path to a device node name.
--
-- @param  device_node  Device node name, e.g., sda1.
-- @return True, or, in case of error, false.
--         The full /dev/... device path, or nil in case of error.
--         The partition type, one of PARTTYPE's values, or nil in case of error.
local function get_dev_path(device_node)
    local function test_dev_path(device_path, partition_type)
        local node = io.open(device_path, "rb")
        if node then
            node:close()
            return true, device_path, partition_type
        end
        return false, nil, nil
    end

    local res, device_path, partition_type
    if device_node:match("ubi%d+:%S+") then
        return true, device_node, PARTTYPE.UBI
    end
    res, device_path, partition_type = test_dev_path(string.format("/dev/disk/by-partuuid/%s", device_node), PARTTYPE.UUID)
    if res then return res, device_path, partition_type end
    res, device_path, partition_type = test_dev_path(string.format("/dev/%s", device_node), PARTTYPE.PLAIN)
    if res then return res, device_path, partition_type end

    swupdate.error("%s: Cannot access target device node /dev/{,disk/by-partuuid}/%s", logprefix, device_node)
    return false, nil, nil
end


--- Execute pre/post-handler function(s) as specified in image.properties.
--
-- @param  when   One of WHEN's values.
-- @param  image  The `struct img_type` Lua Table equivalent.
-- @return True, or, in case of error, nil.
function execute_prepost(when, image)
    if not image.properties then
        return true
    end
    if _VERSION == "Lua 5.1" then _ENV = _G end
    for func, funcparam in pairs(image.properties) do
        local funcname = string.format("pp_%s", func)
        if _ENV[funcname] and type(_ENV[funcname]) == 'function' then
            swupdate.info("%s: Executing on%s function '%s()'.", logprefix, when, func)
            if not _ENV[funcname](when, funcparam) then
                swupdate.error("%s: on%s function '%s()' failed.", logprefix, when, func)
                return nil
            end
        end
    end
    return true
end


--- Parse & Load INI-style configuration.
--
-- @param  cfgtbl  rrconfig Table.
-- @return rrconfig.values, or, in case of error, nil.
local function load_config(cfgtbl)
    -- Return config right away if it has been parsed.
    if cfgtbl.values then
        return cfgtbl.values
    end

    -- Get INI-style string.
    local cfgstr = cfgtbl.builtin
    if not cfgstr then
        cfgtbl.file = cfgtbl.file and cfgtbl.file or "/dev/null"
        swupdate.trace("%s: No compiled-in config found, trying %s", logprefix, cfgtbl.file)
        local file = io.open(cfgtbl.file, "r")
        if not file then
            swupdate.error("%s: Cannot open config file %s", logprefix, cfgtbl.file)
            return nil
        end
        cfgstr = file:read("*a")
        file:close()
    end
    if cfgstr:sub(-1) ~= "\n" then
        cfgstr=cfgstr.."\n"
    end

    -- Parse INI-style string's content into Lua Table.
    cfgtbl.values = {}
    local cfgsec, key, value
    for line in cfgstr:gmatch("(.-)\n") do
        if line:match("^%[([%w%p]+)%][%s]*") then
            line = line:match("^%[([%w%p]+)%][%s]*")
            cfgsec = cfgtbl.values
            for subsec in line:gmatch("(%w+)") do
                cfgsec[subsec] = cfgsec[subsec] and cfgsec[subsec] or {}
                cfgsec = cfgsec[subsec]
            end
        elseif cfgsec then
            key, value = line:match("^([%w%p]-)=(.*)$")
            if key and value then
                value = tostring(value):match("^%s*(.-)%s*$")
                if tonumber(value)  then value = tonumber(value) end
                if value == "true"  then value = true            end
                if value == "false" then value = false           end
                cfgsec[key] = value
            else
                if not line:match("^$") and not line:match("^#") then
                    swupdate.warn("%s: Syntax error, skipping '%s'", logprefix, line)
                end
            end
        else
            swupdate.error("%s: Syntax error. No [section] encountered.", logprefix)
            return nil
        end
    end

    return cfgtbl.values
end


--- Fixups for particular chain-callee handlers.
--
-- Some chain-callee handlers such as the rdiff_image handler require
-- specific fixups in the `struct img_type` Lua Table equivalent passed
-- to them.
--
-- @param  image      The `struct img_type` Lua Table equivalent.
-- @param  rrtarget   Target, i.e., the result of an algorithm's calculation.
-- @param  rrtargets  `image.device` string parse_csv()'d table.
-- @return True, or, in case of error, nil.
--         The modified `struct img_type` Lua Table equivalent.
local handler_fixups = {}
function handler_fixups.rdiff_image(image, rrtarget, rrtargets)
    if image.properties.rdiffbase then
        swupdate.error("%s: rdiffbase= present in properties, use device= mapping instead.", logprefix)
        return nil, image
    end
    -- Reverse the mapping for setting rdiffbase
    local res, rdiffbase, _ = get_dev_path(rrtarget.rrtarget)
    if not res then
        return nil, image
    end
    image.properties.rdiffbase = rdiffbase
    rrtarget.rrtarget = rrtargets.map[rrtarget.rrtarget]
    swupdate.info("%s: Using '%s' as rdiffbase.", logprefix, image.properties.rdiffbase)
    return true, image
end
setmetatable(handler_fixups, {
    __index = function()
        return function(item) return true, item end
    end
})


--- Round Robin Selectors, Sources, Algorithms, and Callbacks Table.
local selector = {
    callbacks = {
        cmdline = {},
        bootenv = {}
    },
    algorithm = {},
    source    = {},
    cache     = {},
}


--- Algorithms.
-- @param  rrcurrent  Current value to run algorithm with.
-- @param  rrtargets  `image.device` string parse_csv()'d table.
-- @return { rrindex=<rrtargets_index>, rrtarget="<devname>" } or {} on error
function selector.algorithm.rr(rrcurrent, rrtargets)
    local index = rrtargets[rrcurrent] and rrtargets[rrcurrent] % #rrtargets + 1
    if not index then return {} end
    return { rrindex = index, rrtarget = rrtargets[index] }
end

function selector.algorithm.id(rrcurrent, rrtargets)
    local index = rrtargets[rrcurrent] and rrtargets[rrcurrent]
    if not index then return {} end
    return { rrindex = index, rrtarget = rrtargets[index] }
end


--- Sources.
-- @param  key        Key to lookup in, e.g., cmdline or bootenv.
-- @param  rrtargets  `image.device` string parse_csv()'d table.
-- @param  algorithm  One of selector.algorithm's functions.
-- @return key's value, or, in case of error, nil.
--         rrtarget table (see Algorithms).
function selector.source.bootenv(key, rrtargets, algorithm)
    -- luacheck: ignore fvalue
    local fvalue, value = selector.callbacks.bootenv[key](
        swupdate.get_bootenv(key),
        key
    )
    if not value then
        swupdate.error("%s: Error getting key '%s' from bootloader environment.", logprefix, key)
        return nil, {}
    end
    return value, algorithm(value, rrtargets)
end

function selector.source.cmdline(key, rrtargets, algorithm)
    if not selector.cache.cmdline then
        local file = io.open("/proc/cmdline", "r")
        if not file then
            swupdate.error("%s: Cannot open %s.", logprefix, "/proc/cmdline")
            return nil, {}
        end
        selector.cache.cmdline = file:read("*l")
        file:close()
    end

    local cachekeybase = string.format("cmdline_%s", key)
    if selector.cache[cachekeybase] then
        return selector.cache[cachekeybase .. "_value"], algorithm(selector.cache[cachekeybase .. "_value"], rrtargets)
    end

    local fvalue, value = selector.callbacks.cmdline[key](
        selector.cache.cmdline:match(string.format("%%f[%%g]%s=%%g+", key)),
        key
    )
    if not value then
        swupdate.error("%s: Error parsing %s's value from /proc/cmdline.", logprefix, key)
        return nil, {}
    end

    selector.cache[cachekeybase] = string.match(
        string.gsub(selector.cache.cmdline, fvalue:gsub("%-", "%%-"), ""):gsub("%s+", " "), ("^%s*(.-)%s*$")
    )
    selector.cache[cachekeybase .. "_token"] = fvalue
    selector.cache[cachekeybase .. "_value"] = value

    swupdate.debug("%s: Parsed %s=%s from /proc/cmdline", logprefix, key, value)

    return value, algorithm(value, rrtargets)
end


--- Callbacks.
-- @param  item  Returned value from a source query for `key`, e.g., root=/dev/blue.
-- @param  key   Lookup key used to query the source, e.g., root.
-- @return Normalized key=value pair or, in case of error, nil.
--         Normalized query key or, in case of error, nil.
setmetatable(selector.callbacks.bootenv, {
        __index = function()
            return function(item, key)
                if not item then return nil, nil end
                return string.format("%s=%s", key, item), item
            end
        end
    })

function selector.callbacks.cmdline.root(item, key)
    -- Special treatment for `root=...` cmdline argument.
    -- See the Kernel's init/do_mounts.c :: name_to_dev_t() for reference.
    -- TODO: If it's root=/dev/root and /dev/root is a symlink, resolve it.
    return strip_dev_path(item, key .. "=")
end
setmetatable(selector.callbacks.cmdline, {
        __index = function()
            return function(item, key)
                if not item then return nil, nil end
                return item, item:gsub(string.format("^%s=", key), "")
            end
        end
    })


--- Methods.
-- @param  key        See Sources.
-- @param  rrtargets  See Sources.
-- @return Target properties table, can be empty {}, properties:
--            mintargets  number of targets, defaults to 2 for A/B round robin.
--            ignorelist  boolean whether to ignore the list of targets altogether.
--         See Sources for the other return values.
function selector.bootenv_rr(key, rrtargets)
    return {}, selector.source.bootenv(key, rrtargets, selector.algorithm.rr)
end

function selector.bootenv_id(key, rrtargets)
    return {}, selector.source.bootenv(key, rrtargets, selector.algorithm.id)
end

function selector.bootenv_rrmap(key, rrtargets)
    return {}, selector.source.bootenv(key, rrtargets, selector.algorithm.rr)
end

function selector.fixed(key, rrtargets) -- luacheck: no unused args
    _, value = strip_dev_path(key, "")
    return { mintargets=1, ignorelist=true }, value, { rrindex = 1, rrtarget = value }
end

function selector.cmdline_rr(key, rrtargets)
    return {}, selector.source.cmdline(key, rrtargets, selector.algorithm.rr)
end

function selector.cmdline_id(key, rrtargets)
    return {}, selector.source.cmdline(key, rrtargets, selector.algorithm.id)
end

function selector.cmdline_rrmap(key, rrtargets)
    return {}, selector.source.cmdline(key, rrtargets, selector.algorithm.rr)
end


--- Round Robin Handler.
--
-- @param  image  The `struct img_type` Lua Table equivalent.
-- @return 0, or, in case of error, 1.
function handler_roundrobin(image)
    -- Read configuration.
    local config = load_config(rrconfig)
    if not config then
        swupdate.error("%s: Cannot read configuration.", logprefix)
        return 1
    end

    -- Check for mandatory handler 'subtype' in image.property table.
    if not (image.properties or {}).subtype then
        swupdate.error("%s: 'subtype' not present in properties.", logprefix)
        return 1
    end

    -- Check configuration for handler of type 'image.property.subtype'.
    config = config[image.properties.subtype]
    if not config then
        swupdate.error("%s [%s] configuration section absent.", logprefix, image.properties.subtype)
        return 1
    end
    if not ( (config.selector or {}).method and ( (config.selector or {}).value or (config.selector or {}).key ) ) then
        swupdate.error("%s: [%s.selector] section invalid.", logprefix, image.properties.subtype)
        return 1
    end
    if not ( selector[config.selector.method] and type(selector[config.selector.method]) == "function" ) then
        swupdate.error("%s: Selector method '%s' in [%s.selector] not implemented.", logprefix,
            config.selector.method, image.properties.subtype)
        return 1
    end
    setmetatable(image.properties, { __index = config })

    -- Check that the chain-callee handler is available.
    if not swupdate.handler[image.properties.chainhandler] then
        swupdate.error("%s: Unknown chain-callee handler '%s'.", logprefix, image.properties.chainhandler or "<unspecified>")
        return 1
    end

    -- Get round robin target list.
    local rrtargets = parse_csv(image.device, function(item) return item:gsub("^/dev/", "") end)

    -- Determine current option and perform round robin calculation for target.
    local key = config.selector.key and config.selector.key or config.selector.value
    local selprops, rrcurrent, rrtarget = selector[config.selector.method](key, rrtargets)

    local mintargets = selprops.mintargets and selprops.mintargets or 2
    if #rrtargets < mintargets then
        swupdate.error("%s: Specify at least %d targets in the device= property.", logprefix, mintargets)
        return 1
    end
    if not rrcurrent then
        return 1
    end

    if not selprops.ignorelist and not rrtargets[rrcurrent] then
        swupdate.error("%s: '%s' is not in round robin target list: %s", logprefix, rrcurrent, table.concat(rrtargets, " "))
        return 1
    end

    -- Resolve mapping (usually maps to identity if no mapping is given in the sw-description's device= field)
    rrtarget.rrtarget = selprops.ignorelist and rrtarget.rrtarget or rrtargets.map[rrtarget.rrtarget]

    local res
    -- Apply fixups for specific chain-callee handlers.
    res, image = handler_fixups[image.properties.chainhandler](image, rrtarget, rrtargets)
    if not res then
        return 1
    end

    -- Get the target's device path and partition type
    res, rrtarget.rrdevice, rrtarget.rrparttype = get_dev_path(rrtarget.rrtarget)
    rrtarget.rrpartprefix = PARTTYPE[rrtarget.rrparttype]
    if not res then
        return 1
    end

    swupdate.info("%s: Using '%s' as target via '%s' handler.", logprefix, rrtarget.rrdevice, image.properties.chainhandler)

    -- Execute property-defined pre-handler function(s)
    if not execute_prepost(WHEN.PRE, image) then
        return 1
    end

    -- Actually flash the partition.
    local msg
    image.type   = image.properties.chainhandler
    image.device = rrtarget.rrdevice
    res, msg = swupdate.call_handler(image.properties.chainhandler, image)
    if res ~= 0 then
        execute_prepost(WHEN.FAIL, image)
        swupdate.error("%s: Error chain-calling '%s' handler: %s", logprefix, image.properties.chainhandler, (msg or ""))
        return 1
    end

    -- Update the Bootloader environment, if applicable.
    setmetatable(rrtarget, {
        __index = function(_, needle)
            local function chktype(item)
                if type(item) == "string" or type(item) == "number" then
                    return item
                end
            end
            if chktype(rawget(selector.cache, needle))   then return rawget(selector.cache, needle) end
            if chktype(rawget(image.properties, needle)) then return rawget(image.properties, needle) end
            if chktype(rawget(config, needle))           then return rawget(config, needle) end
            if chktype(rawget(image, needle))            then return rawget(image, needle) end
        end
    })

    -- Accommodate index for 0-based minds.
    rrtarget.rrindex = rrtarget.rrindex - 1
    for item, value in pairs(config.bootenv or {}) do
        value, _ = string.gsub(tostring(value):gsub("%-", "%%-"), "%${([%w_]+)}", rrtarget)
        if value:match("%${([%w_]+)}") then
            swupdate.error("%s: Error substituting bootloader environment: %s=%s.", logprefix, item, value)
            swupdate.warn("%s: Continuing anyway, check your configuration!", logprefix)
        else
            swupdate.info("%s: Setting bootloader environment: %s=%s", logprefix, item, value)
            swupdate.set_bootenv(item, value)
        end
    end

    -- Execute property-defined post-handler function(s)
    if not execute_prepost(WHEN.POST, image) then
        return 1
    end

    return 0
end

swupdate.register_handler(
    "roundrobin",
    handler_roundrobin,
    swupdate.HANDLER_MASK.IMAGE_HANDLER + swupdate.HANDLER_MASK.FILE_HANDLER
)


--[[ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ]]
--[[  Pre/Post-Handler Functions                                               ]]
--[[ <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ]]

function pp_mmclock(when, device)
    local filename = string.format("/sys/block/%s/force_ro", device)
    local filehandle = io.open(filename, "wb")
    if not filehandle then
        swupdate.error("%s: Cannot open file %s", logprefix, filename)
        return nil
    end
    filehandle:write(when == WHEN.PRE and 0 or 1)
    filehandle:flush()
    filehandle:close()
    swupdate.info("%s: %s MMC device %s", logprefix, when == WHEN.PRE and "Unlocked" or "Locked", device)
    return true
end

