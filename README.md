# SWUpdate Round Robin Handler

The [SWUpdate](https://github.com/sbabic/swupdate.git) round robin Lua handler implementation is a generic run-time dynamic target selection handler, chain-calling other SWUpdate handlers for the heavy lifting works.
In its simplest form, such can be a round robin target selection in an A/B deployment scenario, i.e., when currently running on system A, flash system B and vice versa, hence the name.


It is built around the idea of *selectors* and *algorithms*:
A selector specifies the source of information an algorithm does the run-time dynamic target calculation and selection on.

As a concrete example, consider the following (valid) configuration
```ini
[image]
chainhandler=raw

[image.selector]
method=cmdline_rr
key=root
```

which specifies an "`image`" to be applied via chain-calling SWUpdate's `raw` handler to a target calculated by `image`'s selector subsection `image.selector`:
The selector's source is the Linux kernel command line arguments `/proc/cmdline` from which the `root=<value>` key-value pair is extracted and subject to a round robin calculation as denoted by the selector + algorithm combination `cmdline_rr`.

Currently implemented selector sources are

* `bootenv` reading a value from the bootloader's environment,
* `fixed` using a static value, and
* `cmdline` reading and parsing `/proc/cmdline` for a value.

Currently implemented algorithms are

* `rr` doing a round robin calculation on a target list based on a selector source's value,
* `rrmap` same as `rr` plus applying a device mapping to the target list entries, and
* `id` being the identity of a selector source's value, i.e., returning the very same value.


The round robin handler is selected as handler for a particular `image` or `file` artifact via the update package's `sw-description` meta information file.
In particular, the artifact's `type` has to be set to `roundrobin` to make the round robin handler responsible for processing this artifact.
The artifact's `properties.subtype` selects a particular configuration of chain-called handlers, selectors and algorithms.
For example, the following `sw-description` snippet matches the above example configuration:
```
...
images: (
  {
    filename = "rootfs.img";
    type = "roundrobin";
    device = "/dev/mmcblk0p1,/dev/mmcblk0p2";
    properties: {
      subtype = "image";
    };
  }
);
...
```
It does a round robin calculation based on the Linux kernel command line argument key-value pair `root=/dev/mmcblk0p{1,2}` and flashes the `rootfs.img` image artifact to the respective other partition `/dev/mmcblk0p{2,1}` via chain-calling SWUpdate's `raw` handler.


## Usage / SWUpdate Integration

First, Lua handler implementation support has to be enabled via the compile-time configuration switch "Handlers in Lua" (`CONFIG_HANDLER_IN_LUA`).
Then, `swupdate_handlers_roundrobin.lua` can either be placed as `swupdate_handlers.lua` in SWUpdate's run-time Lua path or be embedded into the SWUpdate binary via the "Embed Lua handler in SWUpdate binary" (`CONFIG_EMBEDDED_LUA_HANDLER`) switch.
In the latter case, `swupdate_handlers_roundrobin.lua` has to be set as "Lua handler file" (`CONFIG_EMBEDDED_LUA_HANDLER_SOURCE`) or has to be copied as `swupdate_handlers.lua` into SWUpdate's source directory, leaving `CONFIG_EMBEDDED_LUA_HANDLER_SOURCE` at its default setting.

The `swupdate.lua` file is just for reference (e.g., code completion) and to track the `swupdate` Lua module provided by SWUpdate's [`corelib/lua_interface.c`](https://github.com/sbabic/swupdate/blob/master/corelib/lua_interface.c) to the Lua handler implementations.


## Configuration

The round robin handler is configured either via built-in compile-time configuration or an external configuration file loaded at run-time (default: `/etc/swupdate.handler.ini`).
Both follow the same INI-style configuration syntax.
In particular for handlers embedded into the SWUpdate binary, it is reasonable to supply the configuration in embedded form as well via modifying the Lua handler implementation `swupdate_handlers_roundrobin.lua` by uncommenting and filling the line

```lua
-- local configuration = [[ ... ]]
```



Each configuration (main) section referenced to by an artifact's `properties.subtype` selects a particular configuration set ― of which multiple can be defined in one configuration file.
Such a configuration set consists of at least (1) the main section specifying the chain-callee handler and (2) the subsection specifying the selector and algorithm, for example:
```ini
[rootfw]
chainhandler=raw

[rootfw.selector]
method=cmdline_rr
key=root
```
The `bootenv` subsection can be given additionally, specifying modifications to the bootloader environment, continuing the above example
```ini
[rootfw.bootenv]
ustate=1
```
which unconditionally sets the bootloader environment variable `ustate` to `1` after the update package has been installed successfully.


#### Variable Expansion in `bootenv`

In addition to static assignments, the values in a `bootenv` subsection are subject to (limited) variable expansion:
In reference to the above `sw-description` snippet with currently having booted `root=/dev/mmcblk0p1`, an entry
```ini
newroot=${rrdevice}
```
in `rootfw.bootenv` sets the `newroot` bootloader environment variable to the target partition resulting from the round robin algorithm calculation.

The following expandable variables are available by default:

* `${rrpartprefix}` is the partition prefix, e.g., `/dev/`,
* `${rrindex}` is the zero-based target partition index in an artifact's `device=` list, e.g., `1` as index `0` (`/dev/mmcblk0p1`) is currently booted,
* `${rrtarget}` is the device node of the target partition, e.g., `mmcblk0p2`,
* `${rrdevice}` is the fully qualified device node of the target partition, e.g., `/dev/mmcblk0p2`, and
* `${rrparttype}` is the target's partition type with `1` denoting UUID partitions, `2` denoting "plain" partitions, and `3` denoting UBI partitions.

Further expandable variables availability depends on the particular configuration of selectors.
For example, when using the `cmdline` selector source with `key=root` as in the above configuration example, the following additional expandable variables are available:

* `${cmdline}` is the full unparsed content of `/proc/cmdline`,
* `${cmdline_root}` is `${cmdline}` without the `root` key-value pair,
* `${cmdline_root_value}` is the parsed `root=<value>` key-value pair, e.g., `mmcblk0p1`, and
* `${cmdline_root_token}` is the full `root=<value>` key-value pair token, e.g., `root=/dev/mmcblk0p1`.

Note that the `key`'s value in the selector configuration determines the variable names' second part, i.e., `cmdline_${key}`, here it is `root`.
See `rrtarget`'s metatable setup in the handler's Lua source code for implementation details.


#### Round Robin + Device Mapping

The `rrmap` algorithm does a round robin calculation on a target list based on a selector source's value and applies a device mapping to the target list entries.
This is in particular useful, for example, if there's an A/B deployment for secondary artifacts such as a Linux kernel in addition to the A/B deployment scheme for primary artifacts, typically being the root filesystem images.

As a concrete example, consider the following configuration snippet continuing the initial example
```ini
[kernel]
chainhandler=rawfile

[kernel.selector]
method=cmdline_rrmap
key=root
```
and this `sw-description` meta information file snippet:

```
...
files: (
  {
    filename = "vmlinuz";
    path = "vmlinuz";
    type = roundrobin";
    device = "mmcblk0p1->mmcblk0p3,mmcblk0p2->mmcblk0p4";
    filesystem = "vfat";
    properties: {
      subtype = "kernel";
    };
  }
);
...
```
It does a round robin calculation based on the Linux kernel command line argument key-value pair `root=/dev/mmcblk0p{1,2}` and copies the `vmlinuz` file artifact via chain-calling SWUpdate's `rawfile` handler to the (mapped) VFAT partition `/dev/mmcblk0p3` if `/dev/mmcblk0p1` is currently booted or to `/dev/mmcblk0p4` if `/dev/mmcblk0p2` is currently booted, respectively.


## Pre- and Post-Handler Lua Functions

The `properties` of an `images` or `files` artifact entry in the `sw-description` meta information file may state pre/post-handler (Lua) functions, i.e., functions that are called immediately prior to, right after, and in case of the chain-called handler's failure.

An example is the unlocking of an eMMC partition `mmcblk2boot0` containing the U-Boot environment so that SWUpdate can update/write to it:
```
...
properties: {
  mmclock = "mmcblk2boot0";
};
...
```

Per convention, the prefix `pp_` has to be used when implementing pre/post-handler functions to "namespace" them as the round robin handler's `_ENV` is simply searched for functions with a `pp_` prefix by means of the `properties`'s keys.
If such a function is found, it is called as pre/post-handler function with the `properties` key's value as (second) parameter.
Otherwise, it is silently ignored, i.e., if the pre/post-handler function(s) referenced in the `properties` of an `sw-description`'s
`images` or `files` artifact entry is not existing in `_ENV`, no error is raised.
A pre/post-handler function must return `0` if successful and a different value therefrom on failure.


For example, the above snippet translates to the function calls
```lua
mmclock("Pre", "mmcblk2boot0")
```
immediately prior to the chain-called handler's actual work and
```lua
mmclock("Post", "mmcblk2boot0")
```
right thereafter with the functions' first parameter `"Pre"` and `"Post"` designating the call type being a pre- or a post-handler call.
In case of the chain-called handler's failure,
```lua
mmclock("Fail", "mmcblk2boot0")
```
will be called to allow for cleanup "finally" actions.
Note that
```lua
mmclock("Post", "mmcblk2boot0")
```
will *not* be called in this case.


For convenience, the enum-alike `WHEN` defines
`WHEN.PRE = "Pre"`,
`WHEN.POST = "Post"`, and
`WHEN.FAIL = "Fail"`
to be used when writing pre/post-handler functions.


For simplicity, pre/post-handler functions are deliberately restricted to take exactly one (second) parameter in addition to the (first) call type specifying parameter.
If multiple parameters should be passed to a pre/post-handler function, e.g., a CSV parsing may be implemented in the pre/post handler function itself.


## Testing & QA

Unit testing is implemented with the help of [busted](https://olivinelabs.com/busted/), see the test cases in `spec/*_spec.lua`.


## License

Copyright 2021 SIEMENS AG

Released under the [GPL-2.0](https://opensource.org/licenses/GPL-2.0), see [COPYING](COPYING) for details.
